package com.beesons.beesonsmiddleware;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeesonsmiddlewareApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeesonsmiddlewareApplication.class, args);
	}

}
